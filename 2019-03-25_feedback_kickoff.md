# Review
* Time to exchange thoughts: good
* Location: LRZ is good
* Went over the proposals for new events, very interesting
* advertisements: twitter, munichrocs, OSC list, LRZ newsletter, flyers(handsout)

Lessons learned for next time:
* we should try to have a sufficient gap between similar events (e.g. RSE)
* next time: LMU newsletter?
* if we combine a talk with an MunichROCS event, advertise both together 
* rotation with regard to the location

# Next steps
* Call for Presentation with Graduate Centre? [Isolde von Bülow](https://www.graduatecenter.uni-muenchen.de/kontakt/index.html)
* Ask participants who proposed events to plan one of these?
* Tobias will ask the colleagues from the library at the next meeting (8th of April)
* Heidi will talk to ZD.B