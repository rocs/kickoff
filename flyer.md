# What is MunicROCS?
* It stands for Munich Researchers for Open and Credible Science (ROCS)
* The organizational core of this grassroots movement is a mailing list 
* We are open to everyone who wants to promote, discuss or learn about Open Science. 
* Sign up to receive regular updates about events, workshops, discussions, and hackathons to learn how to make your research open!
* All members of the list are welcome to share events, experiences or questions and the list should serve as a support platform for early career researchers.

We at Munich ROCS operate in close collaboration with the LMU Open Science Center (www.osc.lmu.de), but we welcome researchers with any affiliation.

# Join the mailing list!
https://lists.lrz.de/mailman/listinfo/munichrocs

# Come to our kickoff event
January 2019 at the Leibniz Supercomputing Centre (A tour to the new Supercomputer is included!)