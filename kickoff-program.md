# MunichROCS - kickoff meeting 28.01.2019

Munich Researchers for Open and Credible Science (ROCS)

* *who*: everyone who wants to promote, discuss, or learn about open and credible science methods, especially if you are an early career researcher at one of Munich's accademic institutions!
* *when*: 28.01.2019 2 pm
* *where*: [Leibniz Supercomputing Centre](https://www.lrz.de/wir/kontakt/weg/)
* *what*:
    * 2:00 pm Opening Talk: Open Science and Research Software Engineering
    * 3:30 pm Guided tour (SuperMUC-NG)
    * 5:00 pm Get2gether: Open Science speed dating and exchange of thoughts
    * Afterwards: Open Science Beers at Garchinger Augustiner
    
* If you are interested, please register [here](https://terminplaner4.dfn.de/MunichRocs), that we can plan accordingly.
* Any questions? Register at our [mailinglist](https://lists.lrz.de/mailman/listinfo/munichrocs) and ask them!