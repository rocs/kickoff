# Meeting to prepare the MunichROCS kickoff

These are the details:
* Xenia will open the event with a quick explanation what MunichROCS is all about and that we are a grassroots-movement (participation is welcome) - our next orga meeting (25.3.) will be announced.
* DK will introduce Heidi and she will give the talk. DK will moderate the questions after that.
* After a break there will be a guided tour. (15:30 - 17:00, Tobias)
* After the guided tour we will proceed (Tobias will moderate since Felix has to leave):
    * With a world coffee (table with topics, one of us plays the moderator) when there are < 15 people left (prepared by Xenia).
    * With a Scientific Speed Dating when there are > 15 people left (prepared by Tobias).

Catering:
* Tobias and Xenia will bring a cake
* The LRZ will provide coffee and water